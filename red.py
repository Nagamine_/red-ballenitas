from random import random
from math import exp
from csv import reader


def crear_red(array):
	red_neuronal = list()
	for i in range(len(array)-1):
		neuronas = list()
		for j in range (array[i+1]):
			pesos =[]
			for k in range(array[i]):
				pesos.append(random())
			neurona ={
				'pesos':pesos,
				'bias':random()
			}
			neuronas.append(neurona)
		print(neuronas)
		red_neuronal.append(neuronas)
	return red_neuronal
#r = crear_red([2,3,2])

def activar(entradas, neurona):
	valor = 0
	for i in range(len(entradas)):
		valor += entradas[i] * neurona['pesos'][i]
	valor += neurona['bias']
	return valor
	
def sigmoid (num):
	return 1.0/(1.0+ exp(-num))

def sigmoid_derivada(num):
	return num *(1.0-num)

def forward(red,inputs):
	nuevas_entradas = inputs
	for capa in red:
		salidas = []
		for neurona in capa:
			activacion = activar(nuevas_entradas,neurona)
			salida = sigmoid(activacion)
			salidas.append(salida)
			neurona['salida']=salida
		nuevas_entradas = salidas
	return nuevas_entradas

def backpropagation(red,l_rate,inputs,salidas_esperadas):
	salida = forward(red,inputs)
	
	ultima_capa = red[-1]
	for i in range(len(ultima_capa)):
		neurona = ultima_capa[i]
		neurona['delta']=(salidas_esperadas[i]-salida[i])*sigmoid_derivada(salida[i])
		
	for i_capa in reversed(range(len(red)-1)):
		capa_siguiente = red[i_capa + 1]
		capa_actual = red[i_capa]
		for n in range(len(capa_actual)):
			error =0.0
			for neurona in capa_siguiente:
				error += neurona['pesos'][n] * neurona['delta']
			
			capa_actual[n]['delta'] = error * sigmoid_derivada(capa_actual[n]['salida'])
	
	entradas = inputs
	#Actualizar pesos
	for capa in red:
		nuevas_entradas = list()
		for neurona in capa:
			for i_peso in range(len(neurona['pesos'])):
				neurona['pesos'][i_peso] += l_rate*neurona['delta'] * entradas[i_peso]
				neurona['bias'] += l_rate*neurona['delta']
			nuevas_entradas.append(neurona['salida'])
		entradas = nuevas_entradas

def entrenar_red(red,l_rate,data_input,salidas,epocas):
	for epoca in range(epocas):
		error_total =0.0
		for fila in range(len(data_input)):
			resultado = forward(red,data_input[fila])
			for i in range(len(salidas[fila])):
				error_total+= (salidas[fila][i]-resultado[i])**2
			backpropagation(red,l_rate,data_input[fila],salidas[fila])
		print("epoca=%d error=%.3f" % (epoca,error_total))

def predecir(red,inputs):
	res = forward(red,inputs)
	return res.index(max(res))
	
def load_csv(filename):
	dataset = list()
	with open(filename, 'r') as file:
		csv_reader = reader(file)
		for row in csv_reader:
			if not row:
				continue
			dataset.append(row)
	return dataset
 
def get_data(dataset):
	
	inputs = list() 
	outputs = list()
	for fila in dataset:
		nueva_fila = list()
		for i in range(len(fila)-1):
			nueva_fila.append(fila[i])
		outputs.append(fila[-1])
		inputs.append(nueva_fila)
	return {
		'inputs':inputs,
		'outputs':outputs
	}

def crear_equivalencias_outputs(outputs):
	salida = set()
	for i in outputs:
		salida.add(i)
	return list(salida)

def crear_array_salida(outputs,equivalencias):
	salida = list()
	
	for i in outputs:
		fila = [0 for j in range(len(equivalencias))]
		fila[equivalencias.index(i)] = 1
		salida.append(fila)
	return salida
	
def normalizar_valores(inputs):
	salida = list()
	minmax = [[min(column), max(column)] for column in zip(*inputs)]
	for fila in inputs:
		nueva_fila = list()
		for i in range(len(fila)):
			valor = (float(fila[i].strip()) - float(minmax[i][0].strip()))/(float(minmax[i][1].strip()) -float(minmax[i][0].strip()))
			nueva_fila.append(valor)
		salida.append(nueva_fila)
	return salida

def min_max(inputs):
	return [[min(column), max(column)] for column in zip(*inputs)]

def normalizar(input, minmax):
	valores = list()
	print(input)
	print(minmax)
	for i in range(len(input)):
		valor = (input[i] - float(minmax[i][0].strip()))/(float(minmax[i][1].strip()) -float(minmax[i][0].strip()))
		valores.append(valor)
	print(valores)
	return valores
			
def predecir_y_normalizar(red,input,minmax):
	i = normalizar(input,minmax)
	
	return predecir(red,i)
	
def mapear_result(result,equivalencias):
	return equivalencias[result]

def test(red,inputs,outputs):
	aciertos = 0
	fallos = 0
	for i in range(len(inputs)):
		p = predecir(red,inputs[i])
		if(p == outputs[i].index(max(outputs[i]))):
			aciertos+= 1
		else:
			fallos+= 1
	print("aciertos " + str(aciertos))
	print("fallos " +str(fallos))
		
d = load_csv("iris.data")
d = get_data(d)
input = normalizar_valores(d['inputs'])
s =crear_equivalencias_outputs(d['outputs'])
salida = crear_array_salida(d['outputs'],s)
red = crear_red([4,6,6,3])
entrenar_red(red,0.1,input,salida,500)

print("-----")
for capa in red:
	print("++++++++")
	for neurona in capa:
		print(neurona)
print("--------------------")
print(red)
minmax = min_max(d['inputs'])
r = predecir_y_normalizar(red,[4.8,3.0,1.4,0.1],minmax)
rm = mapear_result(r,s)
r2 = predecir_y_normalizar(red,[5.6,3.0,4.1,1.3],minmax)
r2m = mapear_result(r2,s)
r3 = predecir_y_normalizar(red,[4.9,2.5,4.5,1.7],minmax)
r3m = mapear_result(r3,s)
print(rm)
print(r2m)
print(r3m)

test(red,input,salida)