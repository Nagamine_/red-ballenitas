from random import random
from math import exp
from csv import reader


def crear_red(array,funciones):
	red_neuronal = list()
	for i in range(len(array) - 1):
		neuronas = list()
		for j in range(array[i + 1]):
			pesos = []
			for k in range(array[i]):
				pesos.append(random())
			neurona = {
				'pesos': pesos,
				'bias': random()
			}
			neuronas.append(neurona)
		print(neuronas)
		red_neuronal.append({'neuronas':neuronas,'funcion':funciones[i]})
	return red_neuronal


# r = crear_red([2,3,2])

def activar(entradas, neurona):
	valor = 0
	for i in range(len(entradas)):
		valor += entradas[i] * neurona['pesos'][i]
	valor += neurona['bias']
	return valor


def sigmoid(num):
	return 1.0 / (1.0 + exp(-num))


def sigmoid_derivada(num):
	return sigmoid(num) * (1.0 - sigmoid(num))

def relu(num):
	if num >0:
		return num
	else:
		return 0.01*num
		
def relu_derivada(num):
	if num <=0:
		return 0.01
	else:
		return 1

def lineal(num):
	return num

def lineal_derivada(num):
	return 1

def forward(red, inputs):
	nuevas_entradas = inputs
	for capa in red:
		salidas = []
		for neurona in capa['neuronas']:
			activacion = activar(nuevas_entradas, neurona)
			salida = capa['funcion']['f'](activacion)
			salidas.append(salida)
			neurona['salida'] = salida
		nuevas_entradas = salidas
	return nuevas_entradas


def backpropagation(red, l_rate, inputs, salidas_esperadas):
	salida = forward(red, inputs)

	ultima_capa = red[-1]
	for i in range(len(ultima_capa['neuronas'])):
		neurona = ultima_capa['neuronas'][i]
		neurona['delta'] = (salidas_esperadas[i] - salida[i]) * ultima_capa['funcion']['derivada'](salida[i])

	for i_capa in reversed(range(len(red) - 1)):
		capa_siguiente = red[i_capa + 1]
		capa_actual = red[i_capa]
		for n in range(len(capa_actual['neuronas'])):
			error = 0.0
			for neurona in capa_siguiente['neuronas']:
				error += neurona['pesos'][n] * neurona['delta']

			capa_actual['neuronas'][n]['delta'] = error * capa_actual['funcion']['derivada'](capa_actual['neuronas'][n]['salida'])

	entradas = inputs
	# Actualizar pesos
	for capa in red:
		nuevas_entradas = list()
		for neurona in capa['neuronas']:
			for i_peso in range(len(neurona['pesos'])):
				neurona['pesos'][i_peso] += l_rate * neurona['delta'] * entradas[i_peso]
				neurona['bias'] += l_rate * neurona['delta']
			nuevas_entradas.append(neurona['salida'])
		entradas = nuevas_entradas


def entrenar_red(red, l_rate, data_input, salidas, epocas):
	for epoca in range(epocas):
		error_total = 0.0
		for fila in range(len(data_input)):
			resultado = forward(red, data_input[fila])
			for i in range(len(salidas[fila])):
				error_total += (salidas[fila][i] - resultado[i]) ** 2
			backpropagation(red, l_rate, data_input[fila], salidas[fila])
		print("epoca=%d error=%.8f" % (epoca, error_total))


def predecir(red, inputs):
	res = forward(red, inputs)
	return res[0]


def load_csv(filename):
	dataset = list()
	with open(filename, 'r') as file:
		csv_reader = reader(file)
		for row in csv_reader:
			if not row:
				continue
			dataset.append(row)
	return dataset


def prepare_data(dataset):
	inputs = list()
	outputs = list()
	for fila in reversed(range(len(dataset) - 7)):
		nueva_fila = list()
		for i in range(fila + 1, fila + 7):
			nueva_fila.append(float(dataset[i][0]))
		outputs.append([float(dataset[fila][0])])
		inputs.append(nueva_fila)
	return {
		'inputs': inputs,
		'outputs': outputs
	}


def normalizar_valores(inputs):
	salida = list()
	minmax = [3189, 60000]
	for fila in inputs:
		nueva_fila = list()
		for i in range(len(fila)):
			valor = (float(fila[i]) - float(minmax[0])) / (float(minmax[1]) - float(minmax[0]))
			nueva_fila.append(valor)
		salida.append(nueva_fila)
	return salida


def normalizar(input, minmax):
	valores = list()
	for i in range(len(input)):
		valor = (input[i] - float(minmax[0])) / (float(minmax[1]) - float(minmax[0]))
		valores.append(valor)
	return valores

def normalizar_y_predecir(red,input,minmax):
	v = normalizar(input,minmax)
	p = predecir(red,v)
	return mapear_result(p,minmax)

def mapear_result(result, minmax):
	return result * (float(minmax[1]) - float(minmax[0])) + float(minmax[0])


def test(red, inputs, outputs):
	aciertos = 0
	fallos = 0
	for indice in range(len(inputs)):
		p = predecir(red, inputs[indice])
		error = ((p-outputs[indice][0])/p) * 100
		print("Esperado %.15f obtenido %.15f error %.7f" % (outputs[indice][0],p,error))
		if error < 2 and error >-2:
			aciertos += 1
		else:
			fallos += 1
	print("aciertos " + str(aciertos))
	print("fallos " + str(fallos))


d = load_csv("ballenitas data.csv")
d = prepare_data(d)

input = normalizar_valores(d['inputs'])
salida = normalizar_valores(d['outputs'])
fun_relu = {'f':relu,
'derivada':relu_derivada
}
fun_sigmoid = {
'f': sigmoid,
'derivada': sigmoid_derivada
}
fun_lineal = {
'f': lineal,
'derivada': lineal_derivada
}

red = crear_red([6,6,6,1],[fun_relu,fun_relu,fun_lineal])
#red = [[{'pesos': [3.179602052286427, -0.2145373625976808, 0.10198687745031504, -0.2748913911874537, -0.10031209903406768, -0.5002203598619939], 'bias': -0.10548974392111282, 'salida': 0.85677850608053, 'delta': -0.000546784898006795}, {'pesos': [3.693029884824744, 0.8333520844219482, 0.5648288448641786, -1.5257937543399007, 1.0404378167781925, -0.5967697768842759], 'bias': -3.5101608088596103, 'salida': 0.4725248616853617, 'delta': -0.0026599597050364922}, {'pesos': [2.7946075937936414, -0.659429165702747, 0.017079569241407643, -0.7969361170786516, 0.9582022013120782, -0.7067215418003443], 'bias': -1.3766755749479205, 'salida': 0.5082709800302639, 'delta': -0.001808056984040325}, {'pesos': [4.494265216521352, 1.9555847735964311, 0.0789951336316531, -0.22637090204310029, -0.17927387423646263, -0.8678642100455298], 'bias': -0.039047288502349065, 'salida': 0.9872976194908495, 'delta': -1.7733264700267108e-05}, {'pesos': [4.627004397511249, 3.1987082498130466, 2.992391949787787, 2.366622801235822, 1.6264132929158241, 2.033370579838292], 'bias': 0.3082078790431584, 'salida': 0.9999988308728571, 'delta': 2.4530479442003366e-09}, {'pesos': [3.227376453008078, -0.44740677590856753, 0.40800854387098545, 0.365022854375482, 0.7822027525595777, 1.150077364149451], 'bias': -2.379568459031412, 'salida': 0.8818024536923456, 'delta': -0.0005179186628775164}], [{'pesos': [2.051036103709825, 1.7065356039453266, 1.82674113675409, 4.3095761182677315, 5.514653040239528, 1.904104121637006], 'bias': -6.580151055866626, 'salida': 0.9997661323338924, 'delta': -4.127138255146492e-06}, {'pesos': [0.1219670354265877, 0.8889612873020195, 0.18039988284943365, 0.3868244364093575, 0.26943047089744593, 0.4450507675686149], 'bias': -0.17381117117854733, 'salida': 0.8155076241331453, 'delta': 2.1193938255489695e-05}, {'pesos': [1.417183486954123, 3.5378884975963025, 2.213518885294574, 0.3073660616298963, -0.5429717577933781, 1.2750534756147898], 'bias': -5.809000823380085, 'salida': 0.28687589791112506, 'delta': -0.002547240884504843}, {'pesos': [1.989902351717272, 1.269208033623115, 1.1369057470369595, 1.154826693241726, -0.7627973079750866, 1.8694944245510765], 'bias': -1.0696021839279615, 'salida': 0.9789370753095511, 'delta': -0.00017562770694670864}, {'pesos': [1.224364444178923, 0.9068209218126689, 1.0923715941594696, 1.1821625152753648, -0.4474963994498884, 1.871404005126534], 'bias': -1.253274604283483, 'salida': 0.958891869804488, 'delta': -0.0002634902695361052}, {'pesos': [0.19668667069321527, 1.4211336518336368, 1.2971332187822886, 0.12506310836250972, -0.5650302450862962, 1.0581143053652717], 'bias': -3.3458454283851307, 'salida': 0.20518929001568711, 'delta': -0.0008513024348676191}], [{'pesos': [6.256685607405082, -0.05016318257306719, 4.413532921453066, 3.0190016320271313, 2.369196770678435, 1.8502711783808616], 'bias': -11.38163314899369, 'salida': 0.8466307250781491, 'delta': -0.002821087169113091}]]
entrenar_red(red, 0.001, input, salida, 50)
minmax = [3189, 60000]
print("-----")
for capa in red:
	print("++++++++")
	for neurona in capa['neuronas]:
		print(neurona)
print("--------------------")
print(red)

r0 = normalizar_y_predecir(red,
			 [47911.1,48577.79,47153.69,47287.6,47969.51,44807.58],minmax)
print(r0)
print("esperado: 49133.45" )
r = normalizar_y_predecir(red,
			 [50052.65,50349.37,48440.65,49587.03,45135.66,46106.43],minmax)
print("precio de hoy:")
print(r)
r3 = normalizar_y_predecir(red,
			 [49206.85,50052.65,50349.37,48440.65,49587.03,45135.66],minmax)
print("precio de mañana:")
print(r3)

test(red, input, salida)
